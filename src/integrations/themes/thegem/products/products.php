<?php
/**
 * Products.
 *
 * @package SpeedSearch
 */

if ( ! defined( 'ABSPATH' ) ) { // Exit if accessed directly (for wordpress.org validations pass).
    exit;
}

/**
 * Posts HTML integration.
 */
add_filter(
    'speedsearch_opportunity_to_insert_custom_html_for_ids',
    /**
     * Adds posts HTML to data object.
     *
     * @param array  $data      Data object, to "html" field of which products' HTML will be added.
     * @param int[]  $posts_ids Posts IDs.
     * @param string $endpoint  Endpoint name.
     *
     * @return array Data.
     *
     * @throws Exception Exception.
     */
    function ( $data, $posts_ids, $endpoint ) {
        ob_start();

        $args = [
            'post__in'            => $posts_ids,
            'post_type'           => 'product',
            'numberposts'         => - 1,
            'posts_per_page'      => - 1,
            'orderby'             => 'post__in',
            'ignore_sticky_posts' => true,
        ];

        add_action(
            'pre_get_posts',
            function( $query ) use ( $args ) {
                if ( $query->is_post_type_archive( 'product' ) ) {
                    $query->parse_query( $args );
                }
            }
        );

	    // If we use the whole woocommerce.com then the actions will be triggered and styles and scripts loaded, but not on the main page.
	    // So better to use the maximum narrowness until we figure out how to fix this whole shit.
		if ( function_exists( 'thegem_woocommerce_grid_content' ) ) {
			thegem_woocommerce_grid_content( false, 0 );
		}

        $data['html'] = ob_get_clean();

        return $data;
    },
    10,
    3
);
