<?php
/**
 * Custom database tables.
 *
 * @package SpeedSearch
 */

namespace SpeedSearch;

use SpeedSearch\Sync_Data_Feed\Sync_Data_Feed_Hashes;

/**
 * Manages custom DB tables.
 */
final class DB {

	/**
	 * Custom tables prefix.
	 */
	const TABLES_PREFIX = 'speedsearch_';

	/**
	 * Constructor (initialize tables).
	 */
	public function __construct() {
		$this->init_tables();
	}

	/**
	 * Inits tables.
	 */
	public function init_tables() {
		global $wpdb;

		$tables_prefix = self::get_tables_prefix();

		foreach ( self::get_tables() as $table ) {
			$wpdb->{"speedsearch_$table"} = "$tables_prefix$table";
		}
	}

	/**
	 * Creates/modifies database tables for the plugin.
	 *
	 * TODO: Delete the previous 'migrations' logic from other parts of the plugin (as we use DB delta now).
	 *
	 * @see WC_Install::create_tables()
	 */
	public static function db_delta() {
		global $wpdb;
		$wpdb->hide_errors();
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		// DB Delta.
		dbDelta( self::get_schema() );
	}

	/**
	 * Return a list of Cache-Warmer tables.
	 *
	 * Parent tables are above children tables.
	 *
	 * @return array Tables list.
	 */
	public static function get_tables() {
		return [
			'feed_buffer',
			'feed_buffer_regen',
		];
	}

	/**
	 * Return full tables prefix.
	 *
	 * @return string Tables prefix.
	 */
	public static function get_tables_prefix() {
		global $wpdb;
		return $wpdb->prefix . self::TABLES_PREFIX;
	}

	/**
	 * Returns tables schema.
	 */
	public static function get_schema() {
		global $wpdb;

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			$collate = $wpdb->get_charset_collate();
		}

		/**
		 * `hash`             - Hash on the data.
		 * `sha_object`       - Hash on the content (content added via PHP, then processed with CONCAT_WS('|',)).
		 * `sha_meta`         - SHA sum of object metadata.
		 * `initial_feed_gen` - If not, then will be added on the feed regeneration.
		 *
		 * speedsearch_feed_buffer_regen - is the same as the original but used only for the feed regeneration
		 */
		$tables = "
            CREATE TABLE {$wpdb->speedsearch_feed_buffer} (
              id BIGINT UNSIGNED AUTO_INCREMENT,
              `created` DATETIME(6) DEFAULT NOW(6),
              `action` VARCHAR(500) NOT NULL,
              `object_id` BIGINT UNSIGNED NOT NULL,
              `object_type` ENUM('post', 'attribute', 'term', '') NOT NULL,
              `hash` VARCHAR(40) NOT NULL,
              `sha_object` VARCHAR(40) NOT NULL,
              `sha_meta` VARCHAR(40) NOT NULL,
              `data` LONGTEXT NOT NULL,
              `opened` DATETIME(6) DEFAULT NULL,
              `written_at` DATETIME(6) DEFAULT NULL,
              `initial_feed_gen` TINYINT(1) UNSIGNED DEFAULT NULL,
              PRIMARY KEY (id),
              KEY idx_written_at (written_at),
              KEY object_id_index (object_id, object_type),
              KEY idx_opened (opened),
              KEY idx_written (written_at)
            ) $collate ENGINE=InnoDB;
            CREATE TABLE {$wpdb->speedsearch_feed_buffer_regen} (
              id BIGINT UNSIGNED AUTO_INCREMENT,
              `created` DATETIME(6) DEFAULT NOW(6),
              `action` VARCHAR(500) NOT NULL,
              `object_id` BIGINT UNSIGNED NOT NULL,
              `object_type` ENUM('post', 'attribute', 'term', '') NOT NULL,
              `hash` VARCHAR(40) NOT NULL,
              `sha_object` VARCHAR(40) NOT NULL,
              `sha_meta` VARCHAR(40) NOT NULL,
              `data` LONGTEXT NOT NULL,
              `opened` DATETIME(6) DEFAULT NULL,
              `written_at` DATETIME(6) DEFAULT NULL,
              `initial_feed_gen` TINYINT(1) UNSIGNED DEFAULT NULL,
              PRIMARY KEY (id),
              KEY idx_written_at (written_at),
              KEY object_id_index (object_id, object_type),
              KEY idx_opened (opened),
              KEY idx_written (written_at)
            ) $collate ENGINE=InnoDB;
        ";

		return $tables;
	}

	/**
	 * Creates a row in the feed_buffer table.
	 *
	 * This method takes data, and inserts a new row into the feed_buffer table.
	 *
	 * @param string $action      Action (usually the same as webhook).
	 * @param int    $object_id   ID of the object.
	 * @param string $object_type Object type.
	 * @param array  $data        An array of the data to add.
	 * @param string $hash        Hash to add.
	 * @param bool   $on_feed_generation On feed generation (not on real webhook action).
	 * @param string $table_to_use Table to use.
	 *
	 * @return void
	 */
	public static function create_buffer_row(
		string $action,
		int $object_id,
		string $object_type,
		array $data,
		string $hash,
		bool $on_feed_generation = false,
		string $table_to_use = ''
	) {
		global $wpdb;

		$table_to_use = $table_to_use ? $table_to_use : $wpdb->speedsearch_feed_buffer;

		// Calculate sha_object based on fields_for_sha1.
		$concat_fields    = implode(
			', ',
			array_map(
				function( $v ) {
					return 't2.' . $v;
				},
				'post' === $object_type ?
					Sync_Data_Feed_Hashes::PRODUCT_HASH_FIELDS :
					(
					'term' === $object_type ?
						Sync_Data_Feed_Hashes::TERM_HASH_FIELDS :
						Sync_Data_Feed_Hashes::ATTRIBUTES_HASH_FIELDS // Attribute.
					)
			)
		);
		$sha1_calculation = "SHA1(CONCAT_WS('|', $concat_fields))";

		if ( 'post' === $object_type ) {
			$meta_keys_whitelist_string = "'" . implode( "', '", Sync_Data_Feed_Hashes::PRODUCT_META_HASH_FIELDS ) . "'";

			$query = $wpdb->prepare(
				"
                INSERT INTO {$table_to_use} (action, object_id, object_type, data, hash, sha_object, sha_meta, initial_feed_gen)
                SELECT %s, %s, %s, %s, %s, $sha1_calculation, SHA1(GROUP_CONCAT(CONCAT_WS('|', t3.meta_key, t3.meta_value) ORDER BY t3.meta_id ASC SEPARATOR '|')), %d
                FROM {$wpdb->posts} t2
                JOIN {$wpdb->postmeta} t3 ON t2.ID = t3.post_id AND t3.meta_key IN ($meta_keys_whitelist_string)
                WHERE t2.ID = %d
                ",
				$action,
				$object_id,
				$object_type,
				maybe_serialize( $data ),
				$hash,
				$on_feed_generation,
				$object_id
			);
		} elseif ( 'term' === $object_type ) {
			$meta_keys_whitelist_string = "'" . implode( "', '", Sync_Data_Feed_Hashes::TERM_META_HASH_FIELDS ) . "'";

			$query = $wpdb->prepare(
				"
                INSERT INTO {$table_to_use} (action, object_id, object_type, data, hash, sha_object, sha_meta, initial_feed_gen)
                SELECT %s, %s, %s, %s, %s, $sha1_calculation, SHA1(GROUP_CONCAT(CONCAT_WS('|', t3.meta_key, t3.meta_value) ORDER BY t3.meta_id ASC SEPARATOR '|')), %d
                FROM {$wpdb->terms} t2
                JOIN {$wpdb->termmeta} t3 ON t2.term_id = t3.term_id AND t3.meta_key IN ($meta_keys_whitelist_string)
                WHERE t2.term_id = %d
                ",
				$action,
				$object_id,
				$object_type,
				maybe_serialize( $data ),
				$hash,
				$on_feed_generation,
				$object_id
			);
		} else { // Attribute.
			$query = $wpdb->prepare(
				"
                INSERT INTO {$table_to_use} (action, object_id, object_type, data, hash, sha_object, sha_meta, initial_feed_gen)
                SELECT %s, %s, %s, %s, %s, $sha1_calculation, '', %d
                FROM {$wpdb->prefix}woocommerce_attribute_taxonomies t2
                WHERE t2.attribute_id = %d
                ",
				$action,
				$object_id,
				$object_type,
				maybe_serialize( $data ),
				$hash,
				$on_feed_generation,
				$object_id
			);
		}

		// Execute the query.
		$wpdb->query( $query );
	}

	/**
	 * Truncates feed buffer table.
	 *
	 * @param bool $retain_deleted_rows Whether to retain the product.deleted rows.
	 */
	public static function truncate_feed_buffer_table( bool $retain_deleted_rows = false ) {
		global $wpdb;

		if ( $retain_deleted_rows ) {
			$wpdb->query( "DELETE FROM {$wpdb->speedsearch_feed_buffer} WHERE `action` != 'product.deleted';" ); // @codingStandardsIgnoreLine
			$wpdb->query( "UPDATE {$wpdb->speedsearch_feed_buffer} SET `written_at` = NULL WHERE `action` = 'product.deleted';" ); // @codingStandardsIgnoreLine
		} else {
			$wpdb->query( "TRUNCATE TABLE {$wpdb->speedsearch_feed_buffer}" ); // @codingStandardsIgnoreLine
		}
	}

	/**
	 * Truncates feed buffer regen table.
	 */
	public static function truncate_feed_buffer_regen_table() {
		global $wpdb;

		$wpdb->query( "TRUNCATE TABLE {$wpdb->speedsearch_feed_buffer_regen}" ); // @codingStandardsIgnoreLine
	}

	/**
	 * Gets the count of rows in the feed_buffer table.
	 *
	 * @param string $table_to_use Table to use.
	 *
	 * @return int The count of rows.
	 */
	public static function get_feed_buffer_count( string $table_to_use = '' ): int {
		global $wpdb;

		$table_to_use = $table_to_use ? $table_to_use : $wpdb->speedsearch_feed_buffer;

		$count = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_to_use}" ); // Use get_var to get the first value from the first row of the result set.

		return (int) $count;
	}

	/**
	 * Gets the count of rows in the feed_buffer table without the product deleted.
	 *
	 * @param string $table_to_use Table to use.
	 *
	 * @return int The count of rows.
	 */
	public static function get_feed_buffer_count_without_product_deleted( string $table_to_use = '' ): int {
		global $wpdb;

		$table_to_use = $table_to_use ? $table_to_use : $wpdb->speedsearch_feed_buffer;

		// Use get_var to get the first value from the first row of the result set.
		$count = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_to_use} WHERE `action` != 'product.deleted';" );

		return (int) $count;
	}

	/**
	 * Swaps the content of speedsearch_feed_buffer and speedsearch_feed_buffer_regen tables.
	 */
	public static function swap_tables() {
		global $wpdb;

		$temp_table = $wpdb->prefix . 'speedsearch_temp';

		// Rename the original tables to temporary names and swap them.
		$wpdb->query(
			"RENAME TABLE {$wpdb->speedsearch_feed_buffer} TO $temp_table,
            {$wpdb->speedsearch_feed_buffer_regen} TO {$wpdb->speedsearch_feed_buffer}, 
            $temp_table TO {$wpdb->speedsearch_feed_buffer_regen}"
		);

		SpeedSearch::$options->set( 'last-tables-swap-done-at', time() );
		SpeedSearch::$options->set( 'last-tables-last-swap-db-error', $wpdb->last_error );
	}
}
