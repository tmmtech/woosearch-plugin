<?php
/**
 * A class for posts data final output.
 *
 * 1. A try_to_inject_html, which is a wrapper for "speedsearch_opportunity_to_insert_custom_html_for_ids" filter.
 * 2. A get_posts_data,     which is a wrapper for @see Posts::get_post_data.
 *
 * A good place to use plugins' filters to fix incompatibility bugs the speedsearch products html generation.
 *
 * Should be extended as rare as possible, only when there is no other way to fix the bug.
 *
 * @package SpeedSearch
 */

namespace SpeedSearch;

use Exception;

/**
 * Class Posts_Data_Final_Output.
 */
final class Posts_Data_Final_Output {

    /**
     * Class to add to the products.
     */
    const SPEEDSEARCH_PRODUCTS_CLASS = 'speedsearch-single-post';

    /**
     * The list of filters to use, with their handlers.
     *
     * 0 - filter
     * 1 - callback
     * 3 - (optional) priority
     * 4 - (optional) accepted arguments
     */
    const FILTERS_TO_USE = [
        [
            'jetpack_lazy_images_new_attributes',
            [ __CLASS__, 'jetpack_disable_products_images_lazyloading' ],
        ],
        [
            'wp_get_attachment_image_attributes',
            [ __CLASS__, 'when_no_image_alt_use_product_title' ],
        ],
        [
            'wp_get_attachment_image_attributes',
            [ __CLASS__, 'add_onerror_to_image' ],
        ],
        [
            'woocommerce_post_class',
            [ __CLASS__, 'product_classes' ],
        ],
        [
            'post_class',
            [ __CLASS__, 'post_classes' ],
            99,
            3,
        ],
    ];

    /**
     * Adds 'speedsearch-single-post' class to each product.
     *
     * @param array $classes Classes list.
     *
     * @return array
     */
    public static function product_classes( $classes ) {
        if ( ! in_array( self::SPEEDSEARCH_PRODUCTS_CLASS, $classes, true ) ) {
            $classes[] = self::SPEEDSEARCH_PRODUCTS_CLASS;
        }

        // TODO: Add 'first' and 'last' classes to products in the columns dynamically, the same way as wc_get_loop_class does it, to have more classes parity.
        // TODO: But without saving it to HTML cache for that. Ideally, during the very late-stage render time.

        return array_filter(
            $classes,
            function( $class ) {
                /**
                 * Delete "last" class because it adds too much randomness to the products HTML cache,
                 * And makes cache validation to stop working (because some products have 'last' class, some do not).
                 */
                return ! in_array( $class, [ 'first', 'last' ], true );
            }
        );
    }

    /**
     * Adds 'speedsearch-single-post' class to each product.
     *
     * @param string[] $classes   An array of post class names.
     * @param string[] $css_class An array of additional class names added to the post.
     * @param int      $post_id   The post ID.
     *
     * @return array
     */
    public static function post_classes( $classes, $css_class, $post_id ) {
        if (
            ! in_array( self::SPEEDSEARCH_PRODUCTS_CLASS, $classes, true ) &&
            'product' === get_post_type( $post_id ) &&
            // For "thegen" theme for example.
            ! in_array( 'image', $classes, true ) &&
            ! in_array( 'caption', $classes, true )
        ) {
            $classes[] = self::SPEEDSEARCH_PRODUCTS_CLASS;
        }

        // TODO: Add 'first' and 'last' classes to products in the columns dynamically, the same way as wc_get_loop_class does it, to have more classes parity.
        // TODO: But without saving it to HTML cache for that. Ideally, during the very late-stage render time.

        return array_filter(
            $classes,
            function( $class ) {
                /**
                 * Delete "last" class because it adds too much randomness to the products HTML cache,
                 * And makes cache validation to stop working (because some products have 'last' class, some do not).
                 */
                return ! in_array( $class, [ 'first', 'last' ], true );
            }
        );
    }

    /**
     * Disables Jetpack products lazy-loading due to the bug.
     *
     * When it keeps this srcset (srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7")
     * even when the image finished lazy-loading.
     *
     * I'm not sure about the deep reasons of this bug, maybe there are much elegant ways to solve it, but I decided to simply
     * disable srcset for products, as for now.
     *
     * @param array $attributes Attributes.
     *
     * @return array Attributes.
     */
    public static function jetpack_disable_products_images_lazyloading( $attributes ) {
        $attributes['srcset'] = '';
        return $attributes;
    }

    /**
     * When the product image has no alt, use product title as the alt.
     *
     * @param array $image_attributes Image HTML.
     *
     * @global $product
     *
     * @return array Image attributes.
     *
     * @throws Exception Exception.
     */
    public static function when_no_image_alt_use_product_title( array $image_attributes ) {
        if ( '1' === SpeedSearch::$options->get( 'setting-when-no-image-alt-use-product-title' ) ) {
            global $product;

            if ( ! $product ) {
                return $image_attributes;
            }

            if ( ! property_exists( $product, 'speedsearch_image_count' ) ) {
                $product->speedsearch_image_count = 0;
            }

            $product->speedsearch_image_count ++;

            if ( '' === $image_attributes['alt'] ) {
                $image_attributes['alt'] = $product->get_title() .
                    ( $product->speedsearch_image_count > 1 ? ' ' . $product->speedsearch_image_count : '' );
            }
        }

        return $image_attributes;
    }

    /**
     * Add onerror attribute to image to reset image thumbnail to null when no available.
     *
     * @param array $image_attributes Image HTML.
     *
     * @global $product
     *
     * @return array Image attributes.
     *
     * @throws Exception Exception.
     */
    public static function add_onerror_to_image( array $image_attributes ) {
        if ( ! isset( $image_attributes['onerror'] ) ) {
            $image_attributes['onerror'] = "this.onerror=null; this.src=speedsearch.imageUrls.placeholderImage; this.removeAttribute('srcset');";
        }

        return $image_attributes;
    }

    /**
     * Extracts posts container HTML from posts HTML with containers block.
     *
     * Extracts the HTML content between the first element with the 'speedsearch-single-post' class
     * and its closest parent with the 'products' class in a given HTML string.
     *
     * And adds "speedsearch-posts" class to the deepest level element.
     *
     * @param string $html The HTML content to be parsed and searched.
     *
     * @return string The inner HTML content between the 'speedsearch-single-post' element and the 'products' parent element.
     *                Returns an empty string if no matching 'products' parent is found.
     */
    public static function get_posts_container_html_from_posts_html_with_containers_block( string $html ) : string {
        // Load the HTML into a DOMDocument.
        $dom = new \DOMDocument();
        // Suppress warnings due to malformed HTML.
        libxml_use_internal_errors( true );
        $dom->loadHTML( $html );
        libxml_clear_errors();

        // Create XPath to navigate the DOM.
        $xpath = new \DOMXPath( $dom );

        // Find the first element with class 'speedsearch-single-post'.
        $speedsearch_post = $xpath->query( "//*[contains(@class, 'speedsearch-single-post')]" )->item( 0 );

        if ( ! $speedsearch_post ) {
            return ''; // Return empty string if no 'speedsearch-single-post' element is found.
        }

        // Traverse up the DOM to find the closest .products parent.
        $products_parent = null;
        // Start from the parent of the speedsearch_post.
        $parent = $speedsearch_post->parentNode;
        $path   = [];
        while ( $parent && $parent->nodeType === XML_ELEMENT_NODE ) {
            if ( $parent->hasAttributes() && $parent->attributes->getNamedItem( 'class' ) && strpos( $parent->getAttribute( 'class' ), 'products' ) !== false ) {
                $products_parent = $parent;
                break;
            }
            $path[] = $parent;
            $parent = $parent->parentNode;
        }

        // If no 'products' parent is found, use the 'speedsearch-single-post' element itself.
        if ( ! $products_parent ) {
            $products_parent = $speedsearch_post;
            $path            = []; // Reset the path as we're starting from the speedsearch_post.
        }

        $new_dom         = new \DOMDocument();
        $current_element = $new_dom->importNode( $products_parent, false );
        $new_dom->appendChild( $current_element );

        // Rebuild the path from top to bottom.
        for ( $i = count( $path ) - 1; $i >= 0; $i-- ) {
            $next_element = $new_dom->importNode( $path[ $i ], false );
            $current_element->appendChild( $next_element );
            $current_element = $next_element;

            // If this is the deepest element, add the 'speedsearch-posts' class.
            if ( 0 === $i ) {
                $existing_class = $current_element->getAttribute( 'class' );
                $new_class      = trim( $existing_class . ' speedsearch-posts' );
                $current_element->setAttribute( 'class', $new_class );
            }
        }

        // If path is empty, add the 'speedsearch-posts' class to the current element.
        if ( empty( $path ) ) {
            $existing_class = $current_element->getAttribute( 'class' );
            $new_class      = trim( $existing_class . ' speedsearch-posts' );
            $current_element->setAttribute( 'class', $new_class );
        }

        // Return the outer HTML of the direct path.
        return $new_dom->saveHTML( $new_dom->documentElement );
    }

    /**
     * Filters out irrelevant HTML and leaves only posts list.
     *
     * Filters out posts that have no HTML within, and keeps only elements
     * that have the specified class and have child nodes.
     *
     * Will result in the fewer data transferred from WP to FE.
     *
     * @param string $html Input HTML to be filtered.
     *
     * @return string Filtered HTML containing only relevant posts with children.
     */
    public static function filter_out_irrelevant_html_and_leave_only_posts( $html ) {
        $elements_with_class_to_retain = self::SPEEDSEARCH_PRODUCTS_CLASS;

        // Load the HTML into a DOMDocument.
        $dom = new \DOMDocument();
        // Suppress warnings due to malformed HTML.
        libxml_use_internal_errors( true );
        $dom->loadHTML( $html );
        libxml_clear_errors();

        $xpath             = new \DOMXPath( $dom );
        $filtered_elements = '';

        $elements = $xpath->query( "//*[contains(concat(' ', normalize-space(@class), ' '), ' {$elements_with_class_to_retain} ')]" );

        foreach ( $elements as $element ) {
            $has_real_children = false;
            foreach ( $element->childNodes as $child ) {
                if ( XML_ELEMENT_NODE === $child->nodeType ) {
                    $has_real_children = true;
                    break;
                }
            }

            if ( $has_real_children ) {
                $filtered_elements .= $dom->saveHTML( $element );
            }
        }

        return $filtered_elements;
    }

    /**
     * Adds posts HTML to data object.
     *
     * @param array  $data      Data object, to "html" field of which products' HTML potentially can be added.
     * @param int[]  $posts_ids Posts IDs.
     * @param string $endpoint  Endpoint name.
     *
     * @throws Exception Exception.
     */
    public static function try_to_inject_html( array $data, array $posts_ids, $endpoint = 'search' ) {
        self::before();

        $data = apply_filters( 'speedsearch_opportunity_to_insert_custom_html_for_ids', $data, $posts_ids, $endpoint );

        self::after();

        /**
         * HTML.
         */
        if ( array_key_exists( 'html', $data ) ) {
            $posts_html = self::filter_out_irrelevant_html_and_leave_only_posts( $data['html'] );

            if ( $posts_html ) {
                $posts_container = self::get_posts_container_html_from_posts_html_with_containers_block( $data['html'] );

                $data['html'] = $posts_html;

                if ( $posts_container ) {
                    $data['containerHtml'] = $posts_container;
                }
            } else {
                unset( $data['html'] );
            }
        }

        return $data;
    }

    /**
     * Returns posts data.
     *
     * @param int[] $posts_ids IDs of the posts.
     *
     * @return array Posts data.
     *
     * @throws Exception Exception.
     */
    public static function get_posts_data( $posts_ids ) {
        self::before();

        $data = [];
        foreach ( $posts_ids as $post_id ) {
            $post_data = Posts::get_post_data( $post_id );
            if ( $post_data ) {
                $data[] = $post_data;
            }
        }

        self::after();

        return $data;
    }

    /**
     * Before the call.
     */
    public static function before() {
        foreach ( self::FILTERS_TO_USE as $filter_data ) {
            add_filter(
                $filter_data[0],
                $filter_data[1],
                isset( $filter_data[2] ) ? $filter_data[2] : 10,
                isset( $filter_data[3] ) ? $filter_data[3] : 1
            );
        }
    }

    /**
     * After the call.
     */
    public static function after() {
        foreach ( self::FILTERS_TO_USE as $filter_data ) {
            remove_filter(
                $filter_data[0],
                $filter_data[1],
                isset( $filter_data[2] ) ? $filter_data[2] : 10
            );
        }
    }
}
